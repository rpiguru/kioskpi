# -*- coding: iso8859-15 -*-

import threading
from functools import partial
import os
import time
from kivy.clock import Clock, mainthread
from kivy.lang import Builder
from kivy.properties import StringProperty
from kivy.uix.widget import Widget
from kivy.logger import Logger
import utils.net
from kivymd.card import MDSeparator
from screens.base import BaseScreen
from widgets.dialog import WiFiInfoDialog, WiFiConnectDialog, YesNoDialog, NotificationDialog
from widgets.wifi.wifi_item import WiFiItem

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'network.kv'))


class NetworkScreen(BaseScreen):
    cur_ssid = StringProperty('')
    ap_list = None
    done = False
    popup = None
    tr_scan = None
    _stop = threading.Event()

    def __init__(self, **kwargs):
        super(NetworkScreen, self).__init__(**kwargs)

    def on_enter(self, *args):
        self.done = False
        threading.Thread(target=self.start_background_threads).start()
        super(NetworkScreen, self).on_enter(*args)

    def start_background_threads(self, *args):
        self._stop.clear()
        try:
            if self.tr_scan:
                self.tr_scan.join()
        except RuntimeError:
            pass
        self.tr_scan = threading.Thread(target=self.get_ap_list)
        self.tr_scan.start()

    def get_ap_list(self, *args):
        while not self._stop.isSet():
            Clock.schedule_once(partial(self.show_loading_spinner, True))
            ap_list = utils.net.get_ap_list('wlan0')
            cur_ap_name, mac = utils.net.get_current_ap()

            # Move current AP to the beginning of the list
            if cur_ap_name is not None:
                try:
                    cur_ap = [ap for ap in ap_list if ap['ssid'] == cur_ap_name and ap['address'] == mac][0]
                    ap_list.remove(cur_ap)
                except IndexError:
                    cur_ap_name = None
                    cur_ap = None
            else:
                cur_ap = None

            ap_list = sorted(ap_list, key=lambda k: k['quality'])
            ap_list.reverse()

            if cur_ap_name is not None:
                ap_list = [cur_ap, ] + ap_list
                self.cur_ssid = cur_ap_name
            else:
                self.cur_ssid = ''
            self.ap_list = ap_list
            Clock.schedule_once(partial(self.update_ap_list, cur_ap_name))
            time.sleep(10)

    @mainthread
    def update_ap_list(self, cur_ap_name, *args):
        self.ids.ml_left.clear_widgets()
        self.ids.ml_right.clear_widgets()
        saved_ap_data = utils.net.get_saved_aps()
        for i in range(len(self.ap_list)):
            ap = self.ap_list[i]
            txt_ssid = ap['ssid']
            if txt_ssid == cur_ap_name:
                txt_ssid += '   -   [b][i]Connected[/i][/b]'
            try:
                item = WiFiItem(text=txt_ssid, id=txt_ssid, saved=True if txt_ssid in saved_ap_data.keys() else False,
                                secondary_text=ap['address'] + (' (encrypted)' if ap['encrypted'] else ''),
                                signal_percentage=ap['quality'])
                item.bind(on_forget=partial(self.on_open_forget_dlg, ap['ssid']))
                item.bind(on_release=partial(self.on_btn, ap))
                if i % 2 == 0:
                    self.ids.ml_left.add_widget(item)
                    self.ids.ml_left.add_widget(MDSeparator(height=1))
                else:
                    self.ids.ml_right.add_widget(item)
                    self.ids.ml_right.add_widget(MDSeparator(height=1))
            except ValueError:
                pass
        self.ids.box_scroll.height = self.ids.ml_left.height
        if len(self.ap_list) % 2 == 1:
            self.ids.ml_right.add_widget(Widget(size_hint_y=None, height=80))
        self.show_loading_spinner(False)
        self.done = True

    def on_btn(self, ap_info, *args):
        ssid = ap_info['ssid']
        if self.cur_ssid == ssid:
            self.popup = WiFiInfoDialog()
        elif not ap_info['encrypted']:
            self.show_loading_spinner(True)
            threading.Thread(target=self.connect_open_wifi, args=(ssid, )).start()
            return
        else:
            saved_ap_data = utils.net.get_saved_aps()
            pwd = saved_ap_data[ssid] if ssid in saved_ap_data.keys() else ''
            self.popup = WiFiConnectDialog(ssid=ssid, pwd=pwd)
            self.popup.bind(on_done=self.on_perform_connect)
        self.popup.open()

    def on_perform_connect(self, *args):
        Logger.info('New IP: {}'.format(args[1]))
        threading.Thread(target=self.get_ap_list).start()

    def on_open_forget_dlg(self, ssid, *args):
        dlg = YesNoDialog(message='Really forget {}?'.format(ssid))
        dlg.bind(on_confirm=partial(self.forget_ssid, ssid))
        dlg.open()

    def forget_ssid(self, ssid, *args):
        args[0].dismiss()
        utils.net.forget_ap_on_file(ssid)
        self.on_enter()

    def on_pre_leave(self, *args):
        self._stop.set()
        super(NetworkScreen, self).on_pre_leave(*args)

    def on_btn_back(self):
        self.switch_screen('home', 'left')

    def connect_open_wifi(self, ssid):
        ip = utils.net.connect_to_ap(ssid, None)
        Clock.schedule_once(partial(self.connect_callback, ssid, ip))

    @mainthread
    def connect_callback(self, ssid, ip, *args):
        self.show_loading_spinner(False)
        if ip is None:
            popup = NotificationDialog(message='Failed to connected to {}\nPlease try again later'.format(ssid))
        else:
            utils.net.update_ap_on_file(ssid, '')
            popup = NotificationDialog(message='Connected to {}\nNew IP: {}'.format(ssid, ip))
        popup.open()
        threading.Thread(target=self.get_ap_list).start()

    @mainthread
    def show_loading_spinner(self, val, *args):
        self.ids.loading_spinner.opacity = 1 if val else 0
