# -*- coding: iso8859-15 -*-

import os
import subprocess
import threading
import time
from kivy.animation import Animation
from kivy.clock import Clock, mainthread
from kivy.lang import Builder
from kivy.properties import NumericProperty
from kivy.uix.gridlayout import GridLayout
from kivy.uix.image import Image
from screens.base import BaseScreen
from settings import MAIN_SCREEN_COLS, MAIN_SCREEN_ROWS, IMAGE_RECORDED_FILES, IMAGE_THUMB_FILES, AUTO_START, \
    INIT_SCREEN, DISTANCE_CHECK_INTERVAL, PIN_TRIG, PIN_ECHO, UPDATE_EVENT, CHECK_DISTANCE, DISTANCE_THRESHOLD
from utils.common import kill_process_by_name, is_rpi
from utils.config import get_config_data
from utils.image import verify_event_images, get_preview_size_pos
from utils.net import check_network_connection
from utils.store_selectors import SelectedImages, CurEvent, PreviewMode, get_current_event_name
from utils.video import play_video_on_widget, get_video_length
from widgets.button import ImageButton
from kivy.logger import Logger

if is_rpi():
    from utils.hc_sr04 import DistanceReader


Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'home.kv'))


MAIN_SCREEN_MAX_PHOTOS = MAIN_SCREEN_COLS * MAIN_SCREEN_ROWS


class HomeScreen(BaseScreen):

    preview_image = None
    event_preview = None
    _tr_network = None
    _stop = threading.Event()
    is_connected = False
    clk_event = None
    page_count = NumericProperty(0)
    last_img = None

    def __init__(self, **kwargs):
        super(HomeScreen, self).__init__(**kwargs)
        self._tr_network = threading.Thread(target=self._check_network)
        Clock.schedule_once(lambda dt: self._add_images())
        self.nav_bar = self.ids.carousel_nav_bar.__self__

    def _add_images(self):
        selected_images = SelectedImages().get()
        if selected_images:
            self.page_count = (len(selected_images) - 1) // MAIN_SCREEN_MAX_PHOTOS + 1
            for k in range(self.page_count):
                grid = GridLayout(cols=MAIN_SCREEN_COLS, rows=MAIN_SCREEN_ROWS, spacing=10, padding=10)
                for i in range(MAIN_SCREEN_MAX_PHOTOS):
                    index = k * MAIN_SCREEN_MAX_PHOTOS + i
                    widget = ImageButton(allow_stretch=True)
                    if index < len(selected_images):
                        widget.source = selected_images[index].replace(IMAGE_RECORDED_FILES, IMAGE_THUMB_FILES)
                    else:
                        widget.disabled = True
                        if self.last_img is None:
                            self.last_img = widget
                    widget.bind(on_release=self.on_image_pressed)
                    grid.add_widget(widget)
                self.ids.carousel.add_widget(grid)
        if self.page_count < 2:     # Remove navigation bar
            self.ids.box.remove_widget(self.nav_bar)

    def on_enter(self, *args):
        if AUTO_START:
            Clock.schedule_once(lambda dt: self.on_auto_start(), 10)
        Clock.schedule_once(lambda dt: self._update_widgets())
        self._stop.clear()
        try:
            self._tr_network.join()
        except RuntimeError:
            pass
        self._tr_network = threading.Thread(target=self._check_network)
        self._tr_network.start()
        self._check_event()
        self.clk_event = Clock.schedule_interval(lambda dt: self._check_event(), 10)
        if is_rpi() and CHECK_DISTANCE:
            threading.Thread(target=self._check_distance).start()
        super(HomeScreen, self).on_enter(*args)

    def _update_widgets(self):
        cur_event = CurEvent().get()
        self.ids.signage.text = cur_event.get('Signage', '')
        self.ids.info_button.opacity = 1 if cur_event['@id'] == 'default' else 0
        self.ids.info_button.disabled = False if cur_event['@id'] == 'default' else True

    def _check_network(self):
        while not self._stop.isSet() and UPDATE_EVENT:
            network_connected = check_network_connection()
            # Update status only when connection state is changed.
            if network_connected != self.is_connected:
                self.is_connected = network_connected
                Clock.schedule_once(lambda dt: self.update_network_status())
            time.sleep(3)

    @mainthread
    def update_network_status(self):
        self.ids.network_button.source = 'assets/images/globe.png' \
            if self.is_connected else 'assets/images/globe-noconnection.png'

    def on_image_pressed(self, *args):
        if self.preview_image is None:
            original_img = args[0]
            img_source = original_img.source.replace(IMAGE_THUMB_FILES, IMAGE_RECORDED_FILES)
            self.preview_image = Image(source=img_source, size=original_img.size,
                                       size_hint=(None, None), pos=original_img.pos)
            self.add_widget(self.preview_image)
            preview_data = get_preview_size_pos(img_source)
            anim = Animation(size=preview_data['size'], pos=preview_data['pos'], t='out_back')
            # If this image is the snapshot of the action video, play it!
            if os.path.exists(img_source.replace('jpg', 'mp4')):
                Clock.schedule_once(lambda dt: self.play_action_video(), anim.duration - .5)
            anim.start(self.preview_image)
            if self.event_preview:
                self.event_preview.cancel()
            self.event_preview = Clock.schedule_once(lambda dt: self.remove_preview(), 3)

    def remove_preview(self):
        if self.preview_image:
            self.remove_widget(self.preview_image)
            self.preview_image = None

    def on_touch_down(self, touch):
        self._kill_preview()
        if self.preview_image:
            self.remove_preview()
        else:
            super(HomeScreen, self).on_touch_down(touch)

    def on_leave(self, *args):
        kill_process_by_name('omxplayer.bin')
        self._stop.set()
        if self.clk_event:
            self.clk_event.cancel()
            self.clk_event = None
        self.ids.carousel.clear_widgets()
        super(HomeScreen, self).on_leave(*args)

    def play_action_video(self):
        if self.preview_image:
            video_src = self.preview_image.source.replace('jpg', 'mp4')
            preview_data = get_preview_size_pos(self.preview_image.source)
            play_video_on_widget(widget=None, size=preview_data['size'], pos=preview_data['pos'], video_src=video_src)
            Clock.schedule_once(lambda dt: self._kill_preview(), get_video_length(video_src) - .2)

    @staticmethod
    def _kill_preview():
        kill_process_by_name('omxplayer.bin')

    def show_start_button(self, val):
        self.ids.btn_start.disabled = False if val else True
        self.ids.btn_start.opacity = 1 if val else 0
        self.ids.btn_motion.disabled = False if val else True
        self.ids.btn_motion.opacity = 1 if val else 0

    def on_btn_start(self):
        PreviewMode().set(False)
        self.switch_screen('select', 'down')

    def on_btn_motion(self):
        self.switch_screen('action_video', 'up')

    def on_btn_info(self):
        self.switch_screen('events', 'left')

    def on_btn_network(self):
        self.switch_screen('network', 'right')

    def on_btn_motion_video(self):
        self.switch_screen('action_video', 'left')

    def _check_event(self):
        """
        Check new event periodically
        :return:
        """
        cur_event_name = get_current_event_name()
        events = get_config_data()['eventconfig']['event']
        if type(events) != list:
            events = [events]
        for event in events:
            if event['@id'] != 'default':
                if int(event['StartTime']) < time.time() < int(event['EndTime']):
                    if event['@id'] != cur_event_name:
                        Logger.info('Event: New event is started - {}'.format(event['@id']))
                        if verify_event_images(event['@id']):
                            CurEvent().set(event)
                            self.show_start_button(True)
                            Clock.schedule_once(lambda dt: self._update_widgets())
                            return
                        else:
                            Logger.error('Event: Duh, image files are corrupted, ignoring...')
                    else:   # Event is still in progress.
                        return
        if cur_event_name != 'default':
            Logger.info('Event: Current event({}) is over, switching to the default mode.'.format(cur_event_name))
            CurEvent().set_default()
            self.show_start_button(False)

    def on_auto_start(self):
        self.switch_screen(INIT_SCREEN, 'up')

    def _check_distance(self):
        last_play_time = 0
        reader = DistanceReader(TRIG=PIN_TRIG, ECHO=PIN_ECHO)
        while not self._stop.isSet():
            distance = reader.read_distance()
            if 0 < distance < DISTANCE_THRESHOLD and time.time() - last_play_time > DISTANCE_CHECK_INTERVAL:
                Logger.info('Kiosk: Person(Object) detected. Playing `hello.mp3`')
                cmd = 'omxplayer -o local assets/sounds/hello.mp3'
                subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                last_play_time = time.time()
            time.sleep(.1)
