# -*- coding: iso8859-15 -*-

import os
import multiprocessing
import threading
from functools import partial
from kivy.animation import Animation
from kivy.clock import Clock, mainthread
from kivy.lang import Builder
from kivy.uix.video import Video
from screens.base import BaseScreen
from settings import PREVIEW_TIMEOUT, IMAGE_PRINT_FILES, WIDTH, HEIGHT
from utils.common import kill_process_by_name, get_image_name, is_rpi
from utils.image import compose_collage_image, get_template_size
import subprocess
from utils.instagram import post_image_to_instagram
from utils.store_selectors import PreviewMode, CollageImagePath, CurEvent, CurrentEventImages, get_current_event_name, \
    PreviousScreen, ActionVideoPath, SelectedImages
from utils.twitter import post_image_to_twitter
from utils.video import get_video_length, get_video_size, create_motion_video

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'thank_you.kv'))


VIDEO_FILE = 'assets/videos/nicole_thanks.mp4'


class ThankYouScreen(BaseScreen):

    return_clock = None
    video = None

    def __init__(self, **kwargs):
        super(ThankYouScreen, self).__init__(**kwargs)
        Clock.schedule_once(lambda dt: self._calibrate_collage_image())

    def on_pre_enter(self, *args):
        super(ThankYouScreen, self).on_pre_enter(*args)
        Clock.schedule_once(self._show_title, 1)
        self.return_clock = None
        Clock.schedule_once(self.start_nicole, .1)
        if PreviousScreen().get() == 'select':
            event_name = get_current_event_name()

            # We need separate 2 paths, otherwise GPU memory usage will be increased!
            # copy.deepcopy() does not work here for some reason, so calling this function twice... :(
            img_path = get_image_name(event_name=event_name, base_path=IMAGE_PRINT_FILES)
            target_path = get_image_name(event_name=event_name, base_path=IMAGE_PRINT_FILES)

            metadata = CurEvent().get()['Print']
            src_images = CurrentEventImages().get()
            p = multiprocessing.Process(target=compose_collage_image, args=(event_name, metadata, src_images, img_path))
            p.start()
            Clock.schedule_once(partial(self.update_image, target_path), get_video_length(VIDEO_FILE) + 1.5)

        elif PreviousScreen().get() == 'action_video':
            video_path = ActionVideoPath().get()
            multiprocessing.Process(target=create_motion_video, args=(video_path, True if is_rpi() else False)).start()
            img_path = video_path.replace('h264', 'jpg')
            old_images = SelectedImages().get()
            old_images.append(img_path)
            SelectedImages().set(old_images)
            Clock.schedule_once(self.on_nicole_finished, get_video_length(VIDEO_FILE) + 1.5)

    def _calibrate_collage_image(self):
        """
        Calibrate the size & position of the collage image
        :return:
        """
        w, h = get_template_size(get_current_event_name())
        padding = 200
        if w / h > WIDTH / HEIGHT:
            self.offset_x = padding  # Offset from the left corner
            width = WIDTH - self.offset_x * 2
            height = width * h / w
            y = (HEIGHT - height) / 2
        else:
            y = padding * h / w
            height = HEIGHT - y * 2
            width = height * w / h
            self.offset_x = (WIDTH - width) / 2
        self.ids.box_img.size = (width, height)
        self.ids.box_img.pos = (WIDTH, y)

    def start_nicole(self, *args):
        x = self.offset_x
        x2 = WIDTH - x
        video_size = get_video_size(VIDEO_FILE)
        height = int((x2 - x) * video_size[1] / video_size[0])
        y = (HEIGHT - height) // 2
        y2 = y + height
        if is_rpi():
            cmd = "omxplayer -o local --aspect-mode letterbox --layer 20 --display 5 --win {},{},{},{} " \
                  "{}".format(x, y, x2, y2, VIDEO_FILE)
            subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        else:
            self.video = Video(source='assets/videos/nicole_thanks.mp4', size_hint=(None, None), volume=0,
                               pos=(x, y), size=(x2 - x, y2 - y))
            self.add_widget(self.video)
            self.video.state = "play"

    def on_leave(self, *args):
        kill_process_by_name('omxplayer.bin')
        if self.return_clock is not None:
            self.return_clock.cancel()
            self.return_clock = None
        super(ThankYouScreen, self).on_leave(*args)

    def on_btn_email(self):
        self.switch_screen('email')

    @mainthread
    def update_image(self, img_path, *args):
        self.ids.img_collage.source = img_path
        CollageImagePath().set(img_path)
        if not PreviewMode().get():
            self.ids.email_button.disabled = False
            self.ids.email_button.opacity = 1
        if not is_rpi():
            self.remove_widget(self.video)
        self.ids.box_img.opacity = 1

        if CurEvent().get().get('Twitter'):
            param = CurEvent().get()['Twitter']
            threading.Thread(target=post_image_to_twitter,
                             kwargs={
                                 'img_path': img_path,
                                 'status': param.get('Msg'),
                                 'app_key': param['APIKey'],
                                 'app_secret': param['APISecret'],
                                 'oauth_token': param['AccessToken'],
                                 'oauth_token_secret': param['AccessSecret'],
                             }).start()

        if CurEvent().get().get('Instagram'):
            param = CurEvent().get()['Instagram']
            threading.Thread(target=post_image_to_instagram,
                             args=(
                                 img_path,
                                 param.get('Msg', ''),
                                 param['Username'],
                                 param['Password']
                             )).start()

        Clock.schedule_once(self.move_collage_image)

    def move_collage_image(self, *args):
        if is_rpi():
            kill_process_by_name('omxplayer.bin')
        anim = Animation(x=self.offset_x)
        anim.start(self.ids.box_img)
        self.return_clock = Clock.schedule_once(lambda dt: self.return_to_home(), PREVIEW_TIMEOUT)

    def on_nicole_finished(self, *args):
        self.switch_screen('home')

    def _show_title(self, *args):
        self.ids.title.opacity = 1
