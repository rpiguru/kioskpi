# -*- coding: iso8859-15 -*-

import multiprocessing
import os
import datetime
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.uix.video import Video
from screens.base import CameraBaseScreen
from settings import IMAGE_RECORDED_FILES, MOTION_VIDEO_MAX_DURATION, NICOLE_VIDEO_START
from utils.common import is_rpi, get_image_name
from utils.image import create_preview_image
from utils.store_selectors import get_current_event_name, ActionVideoPath
from kivy.logger import Logger
from utils.video import play_video_on_widget
from utils.usbrelay import turn_relay

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'action_video.kv'))


class ActionVideoScreen(CameraBaseScreen):

    nicole_video_offset = 2.5
    video_file = 'assets/videos/nicole_record.mp4'
    img_file = ''

    def __init__(self, **kwargs):
        super(ActionVideoScreen, self).__init__(**kwargs)

    def on_enter(self, *args):
        Clock.schedule_once(lambda dt: self.start_nicole(), .1)
        super(ActionVideoScreen, self).on_enter(*args)

    def start_nicole(self):
        """
        Play Nicole at the top-right side.
        :return:
        """
        self.show_preview_on_widget(self.ids.box_preview)
        if is_rpi():
            play_video_on_widget(widget=self.ids.box_collage, video_src=self.video_file)
            turn_relay('on')
        else:
            video_widget = Video(source=self.video_file, volume=0, pos_hint={'x': 0, 'y': 0}, allow_stretch=True)
            self.ids.box_collage.add_widget(video_widget)
            video_widget.state = 'play'
            self.ids.box_collage.opacity = 1
        Clock.schedule_once(lambda dt: self.start_process(), NICOLE_VIDEO_START)

    def start_process(self):
        self.is_recording = True
        if is_rpi():
            self.video_file = get_image_name(event_name=get_current_event_name(),
                                             base_path=IMAGE_RECORDED_FILES, suffix='_m', ext='h264')
            Logger.info('Motion Video: {}: Starting video record.'.format(datetime.datetime.now()))
            self.start_record(self.video_file)
            self.img_file = self.video_file.replace('h264', 'jpg')
            self.take_picture(self.img_file)
            multiprocessing.Process(target=create_preview_image, args=(self.img_file, )).start()
        else:
            self.video_file = 'assets/videos/sample.h264'
        ActionVideoPath().set(self.video_file)
        Clock.schedule_once(lambda dt: self.stop_action_record(), MOTION_VIDEO_MAX_DURATION)

    def stop_action_record(self):
        self.is_recording = False
        self.ids.box_preview.canvas.clear()
        if is_rpi():
            Logger.info('Motion Video: {}: Stopping video record.'.format(datetime.datetime.now()))
            turn_relay('off')
            self.stop_record()
            self.stop_preview()
        Logger.info('Motion Video: {}: Finished'.format(datetime.datetime.now()))
        self.switch_screen('thank_you')
