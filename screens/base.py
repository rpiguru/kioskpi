# -*- coding: iso8859-15 -*-

from functools import partial
from kivy.app import App
from kivy.clock import Clock
from kivy.properties import BooleanProperty
from kivy.uix.screenmanager import Screen
from utils.common import is_rpi, kill_process_by_name
from utils.video import get_video_length


class BaseScreen(Screen):

    app = None

    def __init__(self, **kwargs):
        super(BaseScreen, self).__init__(**kwargs)
        self.app = App.get_running_app()

    def switch_screen(self, screen_name, direction=None):
        self.app.switch_screen(screen_name, direction)

    def return_to_home(self):
        self.switch_screen('home', 'up')


class CameraBaseScreen(BaseScreen):

    is_recording = BooleanProperty(False)
    video_file = 'assets/videos/nicole_photos.mp4'
    nicole_video_offset = 0     # Experimental value for the timing

    def on_enter(self, *args):
        super(CameraBaseScreen, self).on_enter(*args)
        Clock.schedule_once(self.hide_nicole, get_video_length(self.video_file) + self.nicole_video_offset)

    def show_preview_on_widget(self, widget):
        preview_width = int(widget.size[0])
        preview_height = int(widget.size[1])
        preview_x = int(widget.pos[0])
        preview_y = int(self.height - widget.pos[1] - preview_height)
        Clock.schedule_once(partial(self.start_preview, preview_x, preview_y, preview_width, preview_height))

    def take_picture(self, img_path, resolution=None):
        if is_rpi():
            self.app.camera.take_picture(img_path, resolution)

    def start_preview(self, x, y, w, h, *args):
        if is_rpi():
            self.app.camera.start_preview(x, y, w, h)

    def stop_preview(self):
        if is_rpi():
            self.app.camera.stop_preview()

    def start_record(self, img_file):
        self.app.camera.start_record(img_file)

    def stop_record(self):
        self.app.camera.stop_record()

    def on_pre_leave(self, *args):
        kill_process_by_name('omxplayer.bin')
        super(CameraBaseScreen, self).on_pre_leave(*args)

    def hide_nicole(self, *args):
        self.is_recording = False
        if is_rpi():
            kill_process_by_name('omxplayer.bin')
        else:
            self.ids.box_collage.opacity = 0
