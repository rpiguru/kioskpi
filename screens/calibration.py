# -*- coding: iso8859-15 -*-

import os
import threading
import time

import rpyc
from kivy.lang import Builder
from kivy.properties import OptionProperty, BoundedNumericProperty, BooleanProperty, NumericProperty
from screens.base import BaseScreen
from settings import CALIBRATION_IMAGE_PREVIOUS, CALIBRATION_IMAGE_CURRENT
from utils.common import is_rpi
from utils.constant import *
from widgets.calibration.base import CalibrationWidgetBase
from kivy.logger import Logger
from kivy.clock import Clock, mainthread


Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'calibration.kv'))


class CalibrationScreen(BaseScreen):

    awb_mode = OptionProperty('auto', options=AWB_MODE)
    """ Auto-White-Balance Mode
        https://picamera.readthedocs.io/en/release-1.13/api_camera.html#picamera.PiCamera.awb_mode
    """

    brightness = BoundedNumericProperty(50, min=0, max=100)
    """ Brightness
        https://picamera.readthedocs.io/en/release-1.13/api_camera.html#picamera.PiCamera.brightness
    """

    contrast = BoundedNumericProperty(0, min=-100, max=100)
    """ Contrast
        https://picamera.readthedocs.io/en/release-1.13/api_camera.html#picamera.PiCamera.contrast
    """

    drc_strength = OptionProperty('off', options=DRC_MODE)
    """ Dynamic Range Compression Strength
        https://picamera.readthedocs.io/en/release-1.13/api_camera.html#picamera.PiCamera.drc_strength
    """

    exposure_compensation = BoundedNumericProperty(0, min=-25, max=25)
    """ Exposure Compensation Level
        https://picamera.readthedocs.io/en/release-1.13/api_camera.html#picamera.PiCamera.exposure_compensation
    """

    exposure_mode = OptionProperty('auto', options=EXPOSURE_MODE)
    """ Exposure Mode
        https://picamera.readthedocs.io/en/release-1.13/api_camera.html#picamera.PiCamera.exposure_mode
    """

    hflip = BooleanProperty(True)
    """ Horizontal Flip
    """

    image_effect = OptionProperty('none', options=IMAGE_EFFECT)
    """ Image Effect
        https://picamera.readthedocs.io/en/release-1.13/api_camera.html#picamera.PiCamera.image_effect
    """

    meter_mode = OptionProperty('average', options=METER_MODE)
    """ Metering Mode
        https://picamera.readthedocs.io/en/release-1.13/api_camera.html#picamera.PiCamera.meter_mode
    """

    saturation = BoundedNumericProperty(0, min=-100, max=100)
    """ Saturation
        https://picamera.readthedocs.io/en/release-1.13/api_camera.html#picamera.PiCamera.saturation
    """

    sharpness = BoundedNumericProperty(0, min=-100, max=100)
    """ Sharpness
        https://picamera.readthedocs.io/en/release-1.13/api_camera.html#picamera.PiCamera.sharpness
    """

    vflip = BooleanProperty(False)
    """ Vertical Flip
    """

    _last_changed = NumericProperty()
    _is_previous = BooleanProperty(False)

    def on_enter(self, *args):
        super().on_enter(*args)
        Clock.schedule_once(lambda dt: self._start_camera(), .1)

    def _start_camera(self):
        width, height = self.ids.img_current.size
        if is_rpi():
            self.app.camera.start_calibrate_service((int(width), int(height)))
            rpyc_params = self.app.camera.get_current_params()
            params = rpyc.classic.obtain(rpyc_params)
            for p, v in params.items():
                self.ids[p].set_value(v)
            self.update_images()
        for widget in self.walk(restrict=True):
            if isinstance(widget, CalibrationWidgetBase):
                widget.bind(on_changed=self.on_value_changed)

    def on_value_changed(self, *args):
        param = args[0].key
        val = args[0].get_value()
        if time.time() - self._last_changed > .1:
            self._last_changed = time.time()
            Logger.debug('Camera Calibration: {} => {}'.format(param, val))
            if is_rpi():
                self.app.camera.set_param(param, val)
                self.update_images()

    def update_images(self):
        threading.Thread(target=self._capture_image).start()

    def _capture_image(self):
        self.app.camera.take_calibration_picture()
        Clock.schedule_once(lambda dt: self._update_texture())

    @mainthread
    def _update_texture(self):
        if self._is_previous:
            self.ids.img_previous.source = CALIBRATION_IMAGE_PREVIOUS
            self.ids.img_previous.reload()
        self.ids.img_current.source = CALIBRATION_IMAGE_CURRENT
        self.ids.img_current.reload()
        self._is_previous = True

    def on_btn_done(self):
        self.switch_screen('events', 'right')

    def on_pre_enter(self, *args):
        super().on_pre_enter(*args)
        _remove_tmp_images()

    def on_pre_leave(self, *args):
        super().on_pre_leave(*args)
        _remove_tmp_images()
        if is_rpi():
            self.app.camera.stop_service()


def _remove_tmp_images():
    if os.path.exists(CALIBRATION_IMAGE_PREVIOUS):
        os.remove(CALIBRATION_IMAGE_PREVIOUS)
    if os.path.exists(CALIBRATION_IMAGE_CURRENT):
        os.remove(CALIBRATION_IMAGE_CURRENT)
