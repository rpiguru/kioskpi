#!/bin/sh

# Add your SendGrid credentials here.
SENDGRID_USER="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"
SENDGRID_PASSWORD="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"

# Install packages.
sudo apt-get -y install sendmail sendmail-cf heirloom-mailx

# Add SendGrid details to sendmail.mc.
sudo grep smtp.sendgrid.net /etc/mail/sendmail.mc
if [ $? -ne 0 ]; then
  sudo sed -i "s|dnl . Default Mailer setup|\n\
define(\`SMART_HOST', \`smtp.sendgrid.net')dnl\n\
FEATURE(\`access_db')dnl\n\
MASQUERADE_AS(\`tributekiosk.com')dnl\n\
define(\`RELAY_MAILER_ARGS', \`TCP \$h 587')dnl\n\
define(\`ESMTP_MAILER_ARGS', \`TCP \$h 587')dnl\n\
\n\
dnl # Default Mailer setup|" /etc/mail/sendmail.mc

  sudo bash -c 'm4 /etc/mail/sendmail.mc > /etc/mail/sendmail.cf'
fi

# Add SendGrid details to access.db.
sudo grep smtp.sendgrid.net /etc/mail/access
if [ $? -ne 0 ]; then
  sudo bash -c 'cat >> /etc/mail/access <<EOF

# Sendgrid auth credentials
AuthInfo:smtp.sendgrid.net "U:${SENDGRID_USER}" "P:${SENDGRID_PASSWORD}" "M:PLAIN"
EOF'
  sudo bash -c 'makemap hash /etc/mail/access.db < /etc/mail/access'
fi

sudo sed -i "s/127.0.1.1/# 127.0.1.1/g" /etc/hosts
sudo bash -c 'cat >> /etc/hosts <<EOF
127.0.1.1     raspberrypi tributekiosk.com
EOF'

# Sendgrid is very finicky about permissions.
sudo chmod 0755 /etc

# Good to go!
sudo service sendmail restart