# -*- coding: iso8859-15 -*-

CONFIG_FILE = 'raspiConfig.xml'

WIDTH = 1366
HEIGHT = 768


MAIN_SCREEN_COLS = 7
MAIN_SCREEN_ROWS = 5

SELECT_SCREEN_PICTURES_COLS = 4
SELECT_SCREEN_PICTURES_ROWS = 2

IMAGE_LAPSED = "lapsed"
IMAGE_RECORDED_FILES = "recordedfiles"
IMAGE_PRINT_FILES = "printfiles"
IMAGE_THUMB_FILES = 'thumb'

SELECT_TIMEOUT = 20
PREVIEW_TIMEOUT = 10

MOTION_VIDEO_MAX_DURATION = 8       # Max duration of the video in the motion_video page.
NICOLE_VIDEO_START = 11             # Nicole says "Go!" after 11 sec
NICOLE_PHOTO_START = 9.5            # Nicole starts taking photos after 10 sec

PICAMERA_RECORD_WIDTH = 800
PICAMERA_RECORD_HEIGHT = 600
PICAMERA_RECORD_FRAME_RATE = 90

PICAMERA_IMAGE_WIDTH = 1920
PICAMERA_IMAGE_HEIGHT = 1080

RPYC_CAMERA_PORT = 18812

URL_CONNECTIVITY = 'https://files.tributekiosk.com/status.php?id={}'
URL_TEMPLATE = 'https://files.tributekiosk.com/raspiconfig.php?pass=298321&k={}'
REMOTE_CHECK_INTERVAL = 1800

TIME_LAPSED_INTERVAL = 60

SENDGRID_API_KEY = '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'

EMAIL_SENDER = 'nicole@tributekiosk.com'
EMAIL_PASSWORD = ''

TWITTER_APP_KEY = 'gb1ccYa4HZObko4JEW4PrIqmP'
TWITTER_APP_SECRET = 'GCxNnXnI0sCEafv5IdAsDIuTGxOnzQq7CeCyqaNCLMg8YssTSa'
TWITTER_OAUTH_TOKEN = '926485997923250178-jMUJ6ozfrVEQFlIv6PsKtspreYgCVut'
TWITTER_OAUTH_SECRET = 'GTjp6JCY6UyuvJLOghBcl8Rq0eOIKDuN0KnDxcDXeHLfy'

INSTAGRAM_USER = 'instagram_user'
INSTAGRAM_PWD = 'instagram_pwd'

MONGO_DB = 'Kiosk'
MONGO_COLLECTION_EMAIL = 'email'
MONGO_COLLECTION_INSTAGRAM = 'instagram'
MONGO_COLLECTION_TWITTER = 'twitter'

UPDATE_EVENT = True
INIT_SCREEN = 'home'

AUTO_START = False

EVENT_PREPARE_TIME = 50

# Add 25 pixel border before printing the collage image.
BORDER = True

# ===================== Ultra Sonic Sensor =======================
CHECK_DISTANCE = True
PIN_TRIG = 18
PIN_ECHO = 24
DISTANCE_CHECK_INTERVAL = 120       # How often do we check person?
DISTANCE_THRESHOLD = 50


CALIBRATION_IMAGE_PREVIOUS = '/dev/shm/kiosk_previous.jpg'
CALIBRATION_IMAGE_CURRENT = '/dev/shm/kiosk_current.jpg'


try:
    from local_settings import *
except ImportError:
    pass
