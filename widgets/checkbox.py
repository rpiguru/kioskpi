# -*- coding: iso8859-15 -*-

import os
from kivy.uix.gridlayout import GridLayout
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.properties import StringProperty, BooleanProperty

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'checkbox.kv'))


class LabeledCheckbox(GridLayout):

    group = StringProperty('')
    allow_no_selection = BooleanProperty(True)
    text = StringProperty('')

    def __init__(self, **kwargs):
        self.register_event_type('on_checked')
        super(LabeledCheckbox, self).__init__(**kwargs)
        Clock.schedule_once(self.bind_chk)

    def bind_chk(self, *args):
        self.ids.chk.bind(active=self.on_released)

    def on_released(self, *args):
        self.dispatch('on_checked', self.ids.chk.active)

    def on_checked(self, *args):
        pass

    def set_value(self, val):
        self.ids.chk.active = val

    def get_value(self):
        return self.ids.chk.active

    def enable(self, value):
        self.ids.chk.disabled = not value
