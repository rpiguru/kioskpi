# -*- coding: iso8859-15 -*-

from kivy.properties import BooleanProperty
from kivy.uix.textinput import TextInput

from utils.e_mail import is_valid_email


class KioskEmailInput(TextInput):

    is_valid = BooleanProperty()

    def on_text(self, instance, substring):
        if is_valid_email(substring):
            self.background_color = (0, 0, .3, .3)
            self.is_valid = True
        else:
            self.is_valid = False
            if substring != '':
                self.background_color = [.5, 0, 0, .3]
