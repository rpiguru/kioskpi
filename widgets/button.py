# -*- coding: iso8859-15 -*-


from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.image import Image


class ImageButton(ButtonBehavior, Image):
    keep_ratio = False
