# -*- coding: iso8859-15 -*-

import time
from kivy.clock import Clock
import os
from kivy.lang import Builder
from kivy.properties import StringProperty, ListProperty
from kivy.uix.label import Label
from kivy.uix.scrollview import ScrollView

from widgets.base import AnimatedWidget

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'label.kv'))


class ClockWidget(Label):
    def __init__(self, **kwargs):
        super(ClockWidget, self).__init__(**kwargs)

        self.clock_timer = Clock.schedule_interval(self.update_clock, 0.2)

    def update_clock(self, dt):
        currentime = time.strftime("%c")

        if currentime != self.text:
            self.text = currentime


class ColoredLabel(Label, AnimatedWidget):

    def __init__(self, **kwargs):
        super(ColoredLabel, self).__init__(**kwargs)
        self.register_event_type('on_press')

    def on_touch_down(self, touch):
        if self.collide_point(*touch.pos):
            self.dispatch('on_press')

    def on_press(self):
        pass


class H0(ColoredLabel):
    pass


class H1(ColoredLabel):
    pass


class H2(ColoredLabel):
    pass


class H3(ColoredLabel):
    pass


class H4(ColoredLabel):
    pass


class H5(ColoredLabel):
    pass


class H6(ColoredLabel):
    pass


class P(ColoredLabel):
    pass


class ScrollableLabel(ScrollView):
    text = StringProperty('')
    color = ListProperty([1, 1, 1, 1])
