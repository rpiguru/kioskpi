# -*- coding: iso8859-15 -*-
import os
import threading
from kivy.clock import Clock, mainthread
from kivy.lang import Builder
from kivy.properties import StringProperty, NumericProperty, BooleanProperty, ObjectProperty, DictProperty
from kivy.uix.modalview import ModalView
import utils.net
from kivymd.dialog import MDDialog
import utils.config
from functools import partial
from widgets.checkbox import LabeledCheckbox

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'dialog.kv'))


class WiFiInfoDialog(MDDialog):

    ssid = StringProperty('')
    quality = NumericProperty(0)
    ip = StringProperty('', allow_none=True)
    mac_addr = StringProperty('')

    def __init__(self, **kwargs):
        super(WiFiInfoDialog, self).__init__(**kwargs)

    def on_open(self):
        Clock.schedule_once(self.get_wifi_details)

    def get_wifi_details(self, *args):
        wifi = utils.net.get_detail_of_connected_net()
        if wifi is not None:
            self.ssid = wifi['ssid']
            self.quality = int(wifi['quality'])
            self.ip = wifi['ip']
            self.mac_addr = wifi['mac']


class WiFiConnectDialog(MDDialog):

    ssid = StringProperty('')
    pwd = StringProperty('')
    loading_dlg = ObjectProperty(None)

    def __init__(self, **kwargs):
        super(WiFiConnectDialog, self).__init__(**kwargs)
        self.register_event_type('on_done')
        self.ids.txt_pwd.text = self.pwd
        self.loading_dlg = LoadingDialog()

    def on_open(self):
        super(WiFiConnectDialog, self).on_open()
        Clock.schedule_once(self.focus_input, .3)

    def focus_input(self, *args):
        self.ids.txt_pwd.focus = True

    def on_done(self, *args):
        pass

    def on_yes(self, *args):
        pwd = self.ids.txt_pwd.text
        if len(pwd.strip()) == 0:
            self.ids.lb_error.opacity = 1
            Clock.schedule_once(self.hide_error, 2)
            return
        self.pwd = pwd
        Clock.schedule_once(self.show_load)
        threading.Thread(target=self.connect_to_ap).start()

    def show_load(self, *args):
        self.dismiss()
        self.loading_dlg.open()

    def hide_error(self, *args):
        self.ids.lb_error.opacity = 0

    @mainthread
    def hide_load(self):
        self.loading_dlg.dismiss()

    def connect_to_ap(self, *args):
        ip = utils.net.connect_to_ap(self.ssid, self.pwd)
        Clock.schedule_once(partial(self.connect_callback, ip))

    @mainthread
    def connect_callback(self, ip, *args):
        self.hide_load()
        if ip is None:
            popup = NotificationDialog(message='Failed to connected to {}\nPlease try again later'.format(self.ssid))
            # self.ids.container.add_widget(
            #     MDLabel(font_style='Body1', theme_text_color='Error', text="Failed to connect", halign='center'))
        else:
            utils.net.update_ap_on_file(self.ssid, self.pwd)
            popup = NotificationDialog(message='Connected to {}\nNew IP: {}'.format(self.ssid, ip))
        popup.open()
        self.dispatch('on_done', ip)

    # def on_touch_down(self, touch):
    #     super(WiFiConnectDialog, self).on_touch_down(touch)
    #     self.ids.lb_error.opacity = 0


class AuthDialog(MDDialog):
    pwd = StringProperty('')
    mode = StringProperty('')
    is_error = BooleanProperty(False)

    def __init__(self, **kwargs):
        self.register_event_type('on_success')
        super(AuthDialog, self).__init__(**kwargs)
        # self.pwd = utils.config.get_config('password_{}'.format(kwargs['mode']), '')

    def on_open(self):
        self.clear_error()
        self.ids.txt_pwd.text = ''
        self.ids.txt_pwd.focus = True

    def on_success(self, *args):
        pass

    def on_ok(self):
        if self.ids.txt_pwd.text == self.pwd:
            self.dismiss()
            self.dispatch('on_success', self.mode)
        else:
            self.is_error = True

    def clear_error(self):
        self.is_error = False

    def on_touch_down(self, touch):
        if self.is_error:
            self.ids.txt_pwd.text = ''
            self.is_error = False
        super(AuthDialog, self).on_touch_down(touch)


class LoadingDialog(ModalView):
    pass


class NotificationDialog(MDDialog):

    message = StringProperty('')

    def __init__(self, **kwargs):
        super(NotificationDialog, self).__init__(**kwargs)


class YesNoDialog(MDDialog):

    message = StringProperty('')

    def __init__(self, **kwargs):
        self.register_event_type('on_confirm')
        super(YesNoDialog, self).__init__(**kwargs)

    def on_yes(self):
        self.dispatch('on_confirm')

    def on_confirm(self):
        pass


class InputDialog(MDDialog):

    text = StringProperty('')
    hint_text = StringProperty('')

    def __init__(self, **kwargs):
        self.register_event_type('on_confirm')
        super(InputDialog, self).__init__(**kwargs)
        Clock.schedule_once(self.enable_focus, .3)

    def on_yes(self):
        new_val = self.ids.input.text
        self.dismiss()
        self.dispatch('on_confirm', new_val)

    def on_confirm(self, *args):
        pass

    def enable_focus(self, *args):
        self.ids.input.focus = True


class IPInputDialog(MDDialog):

    text = StringProperty('')
    hint_text = StringProperty('')

    def __init__(self, **kwargs):
        self.register_event_type('on_confirm')
        super(IPInputDialog, self).__init__(**kwargs)
        Clock.schedule_once(self.enable_focus, .3)

    def on_yes(self):
        new_val = self.ids.input.text
        self.dismiss()
        self.dispatch('on_confirm', new_val)

    def on_confirm(self, *args):
        pass

    def enable_focus(self, *args):
        self.ids.input.focus = True


class MultiSelectDialog(MDDialog):

    choices = DictProperty()

    def __init__(self, **kwargs):
        super(MultiSelectDialog, self).__init__(**kwargs)
        self.register_event_type('on_finished')
        Clock.schedule_once(self._update_container)

    def _update_container(self, *args):
        self.ids.container.clear_widgets()
        for _key in sorted(self.choices, key=self.choices.get):
            chk = LabeledCheckbox(group=_key, text=self.choices[_key])
            chk.bind(on_checked=self._on_checkbox_checked)
            self.ids.container.add_widget(chk)
            self.ids.container.height += chk.height

    def _on_checkbox_checked(self, *args):
        self.ids.btn_ok.disabled = False if len(self.get_checked_vals().keys()) > 0 else True

    def on_yes(self):
        self.dismiss()
        self.dispatch('on_finished', self.get_checked_vals())

    def on_finished(self, *args):
        pass

    def get_checked_vals(self):
        selected = {}
        for wid in self.ids.container.children:
            if wid.get_value():
                selected.update({wid.group: wid.text})
        return selected


class PasswordDialog(MDDialog):
    text = StringProperty('')
    hint_text = StringProperty('Password')

    def __init__(self, **kwargs):
        self.register_event_type('on_confirm')
        super(PasswordDialog, self).__init__(**kwargs)
        Clock.schedule_once(self.enable_focus, .3)

    def on_yes(self):
        new_val = self.ids.input.text
        self.dismiss()
        self.dispatch('on_confirm', new_val)

    def on_confirm(self, *args):
        pass

    def enable_focus(self, *args):
        self.ids.input.focus = True
