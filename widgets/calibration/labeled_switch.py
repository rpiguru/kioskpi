# -*- coding: iso8859-15 -*-

import os
from kivy.lang import Builder

from widgets.calibration.base import CalibrationWidgetBase


Builder.load_file(os.path.join(os.path.dirname(__file__), 'labeled_switch.kv'))


class LabeledSwitch(CalibrationWidgetBase):

    def _bind_child(self):
        self.ids.switch.bind(active=self.on_switched)

    def set_value(self, val):
        self.ids.switch.active = val

    def get_value(self):
        return self.ids.switch.active

    def on_switched(self, *args):
        self.dispatch('on_changed')
