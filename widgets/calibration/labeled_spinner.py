# -*- coding: iso8859-15 -*-

import os
from kivy.lang import Builder
from kivy.properties import ListProperty
from widgets.calibration.base import CalibrationWidgetBase

Builder.load_file(os.path.join(os.path.dirname(__file__), 'labeled_spinner.kv'))


class LabeledSpinner(CalibrationWidgetBase):

    values = ListProperty()

    def _bind_child(self):
        self.ids.spinner.bind(on_changed=self._on_spinner_changed)

    def _on_spinner_changed(self, *args):
        self.dispatch('on_changed')

    def get_value(self):
        return self.ids.spinner.value

    def set_value(self, val):
        self.ids.spinner.set_value(val)
