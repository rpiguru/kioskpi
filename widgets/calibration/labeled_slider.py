# -*- coding: iso8859-15 -*-

import os
from kivy.lang import Builder
from kivy.properties import StringProperty, NumericProperty
from widgets.calibration.base import CalibrationWidgetBase

Builder.load_file(os.path.join(os.path.dirname(__file__), 'labeled_slider.kv'))


class LabeledSlider(CalibrationWidgetBase):

    icon = StringProperty('check')
    value = NumericProperty(0)
    minimum = NumericProperty(0)
    maximum = NumericProperty(100)

    def _bind_child(self):
        self.ids.slider.value = self.value
        self.ids.slider.bind(value=self._on_slider_changed)

    def _on_slider_changed(self, *args):
        self.value = int(self.ids.slider.value)
        self.dispatch('on_changed')

    def get_value(self):
        return self.value

    def set_value(self, val):
        self.value = val
        self.ids.slider.value = val
