# -*- coding: iso8859-15 -*-

import os
from kivy.lang import Builder

from kivymd.button import MDIconButton
from kivymd.list import TwoLineIconListItem, ILeftBodyTouch

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'event_item.kv'))


class IconLeftSampleWidget(ILeftBodyTouch, MDIconButton):
    pass


class EventItem(TwoLineIconListItem):

    font_style = 'Title'
    secondary_font_style = 'Subhead'
    _txt_left_pad = 90
    _txt_bot_pad = 10

    def __init__(self, **kwargs):
        super(EventItem, self).__init__(**kwargs)
