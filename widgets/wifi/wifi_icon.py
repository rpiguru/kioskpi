# -*- coding: iso8859-15 -*-

import os
from kivy.factory import Factory
from kivy.lang import Builder
from kivy.properties import NumericProperty
from kivy.uix.boxlayout import BoxLayout


Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'wifi_icon.kv'))


class WiFiIcon(BoxLayout):

    strength = NumericProperty(0)

    def __init__(self, **kwargs):
        super(WiFiIcon, self).__init__(**kwargs)

    def set_strength(self, val):
        val = int(val)
        if val == 5:
            val = 4
        self.strength = val

    def get_strength(self):
        return self.strength


Factory.register('WiFiIcon', cls=WiFiIcon)
