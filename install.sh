#!/usr/bin/env bash

echo "Starting installation script..."
sudo apt-get update
sudo apt-get -y dist-upgrade
sudo rpi-update

sudo rm -rf /etc/ssh/ssh_host_* && sudo dpkg-reconfigure openssh-server  # Needed when duplicating OS images.

sudo apt-get install -y libblas-dev liblapack-dev libjpeg-dev libpython3-dev git libatlas-base-dev
sudo apt-get install -y python3-picamera imagemagick python3-pil gpac omxplayer mpg321

echo "Installing CUPS drivers..."
sudo apt-get install -y cups libcups2-dev libcupsimage2-dev g++ cups-client
sudo apt-get install -y printer-driver-gutenprint

echo "Installing Kivy on Raspberry Pi..."
sudo apt-get install -y python3-dev python3-pip
sudo pip3 install -U pip
sudo apt-get install -y libsdl2-dev libsdl2-image-dev libsdl2-mixer-dev libsdl2-ttf-dev \
     pkg-config libgl1-mesa-dev libgles2-mesa-dev \
     python-setuptools libgstreamer1.0-dev git-core gstreamer1.0-plugins-bad gstreamer1.0-plugins-base \
     gstreamer1.0-plugins-good gstreamer1.0-plugins-ugly \
     gstreamer1.0-omx gstreamer1.0-alsa python-dev libmtdev-dev xclip wiringpi

sudo pip3 install Cython==0.25.2
sudo pip3 install git+https://github.com/kivy/kivy.git@master

sudo pip3 install -r requirements.txt

sudo pip3 install -e git+https://github.com/LevPasha/Instagram-API-python.git#egg=InstagramAPI


cur_dir=`dirname $0`
#bash ${cur_dir}/install_opencv.sh
#bash ${cur_dir}/install_ffmpeg.sh

sudo apt-get install -y ffmpeg

echo "Installing mencoder..."
cd ~
sudo apt-get install -y subversion
svn checkout svn://svn.mplayerhq.hu/mplayer/trunk mplayer
cd mplayer
./configure
make -j2
./mencoder
sudo make install
cd ~
sudo rm -r mplayer

echo "Configuring Raspberry Pi..."

# Enable SSH
sudo touch /boot/ssh

# Increase GPU memory size
sudo echo "gpu_mem=512" | sudo tee -a /boot/config.txt

# Enable PiCamera
sudo echo "start_x=1" | sudo tee -a /boot/config.txt

# Install NetworkManager and configure, which is more robust then wpa_supplicant
sudo apt-get install -y network-manager
sudo sed -i -- 's/managed=false/managed=true/g' /etc/NetworkManager/NetworkManager.conf
sudo echo "denyinterfaces wlan0" | sudo tee -a /etc/dhcpcd.conf
# Disable wlan0 configuration for the NetworkManager
sudo sed -i -- 's/allow-hotplug wlan0//g' /etc/network/interfaces
sudo sed -i -- 's/iface wlan0 inet/#iface wlan0 inet/g' /etc/network/interfaces
sudo sed -i -- 's/wpa-conf/#wpa-conf/g' /etc/network/interfaces
sudo sed -i -- 's/wpa-ssid/#wpa-ssid/g' /etc/network/interfaces
sudo sed -i -- 's/wpa-psk/#wpa-psk/g' /etc/network/interfaces

echo "Installing mongoDB on RPi..."
cur_dir="$( cd "$(dirname "$0")" ; pwd -P )"
bash ${cur_dir}/script/install_mongodb.sh

#echo "Disabling Screen Saver..."
#sudo sed -i -- 's/#xserver-command=X/xserver-command=X -s 0 dpms/g' /etc/lightdm/lightdm.conf

echo "Installing KioskPi Service..."
#sudo cp conf/kioskpi.service /etc/systemd/system
#sudo mkdir -p /var/log/kioskpi && sudo chmod 775 /var/log/kioskpi
#sudo mkdir -p /opt/kioskpi && sudo chmod 775 /opt/kioskpi
#sudo cp -r ${cur_dir} /opt/kioskpi
#sudo systemctl enable kioskpi

echo "========== Finished Installation. Rebooting now... =========="
sudo reboot
