"""
    Pre-configuration file to be used to setup Kivy configurations.
    This module must be called before executing a Kivy GUI app!
"""
import os
from kivy.clock import Clock
from kivy.config import Config
from settings import WIDTH, HEIGHT

Config.read(os.path.expanduser('~/.kivy/config.ini'))

Config.set('graphics', 'width', str(WIDTH))
Config.set('graphics', 'height', str(HEIGHT))
Config.set('kivy', 'keyboard_mode', 'systemanddock')
Config.set('kivy', 'keyboard_layout', 'en_US')
# Config.set('modules', 'touchring', 'image=/home/pi/cursor.png,show_cursor=1')

Clock.max_iteration = 20
