# -*- coding: iso8859-15 -*-

from utils.config import get_event_by_name
from utils.store import StoreGetSetter


class CollageImagePath(StoreGetSetter):
    key = 'collage_image_path'
    default_for_missed = ''


class SelectedImages(StoreGetSetter):
    key = 'selected_images'
    default_for_missed = []


class ActionVideoPath(StoreGetSetter):
    key = 'action_video_path'
    default_for_missed = ''


class CurrentEventImages(StoreGetSetter):
    key = 'current_images'
    default_for_missed = []


class CurEvent(StoreGetSetter):
    key = 'current_event'
    default_for_missed = get_event_by_name('default')


class PreviewMode(StoreGetSetter):
    key = 'is_preview_mode'
    default_for_missed = True


class PreviousScreen(StoreGetSetter):
    key = 'previous_screen'


def get_current_event_name():
    return CurEvent().get()['@id']
