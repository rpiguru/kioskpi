# -*- coding: iso8859-15 -*-

import datetime
import threading
import time
import numpy
import os
import rpyc
import shutil
from PIL import Image
from picamera.array import PiRGBArray
from rpyc.utils.server import ThreadedServer
from picamera import PiCamera
from settings import RPYC_CAMERA_PORT, MOTION_VIDEO_MAX_DURATION, CALIBRATION_IMAGE_PREVIOUS, CALIBRATION_IMAGE_CURRENT
from picamera.exc import PiCameraNotRecording


class CameraService(rpyc.Service):

    camera = None
    raw_capture = None

    stop_record = threading.Event()
    params = {
        'awb_mode': 'auto',
        'brightness': 50,
        'contrast': 0,
        'drc_strength': 'off',
        'exposure_compensation': 0,
        'exposure_mode': 'auto',
        'hflip': True,
        'image_effect': 'none',
        'meter_mode': 'average',
        'saturation': 0,
        'sharpness': 0,
        'vflip': False
    }

    def exposed_start_service(self, sensor_mode=1, resolution=(1920, 1028), framerate=30):
        """
        See https://picamera.readthedocs.io/en/release-1.13/fov.html#sensor-modes for more details
        :return:
        """
        print('{} :: Starting Camera Service'.format(datetime.datetime.now()))
        if self.camera is None:
            self.camera = PiCamera(sensor_mode=sensor_mode, resolution=resolution, framerate=framerate)
            self._restore_params()
            self.camera.brightness = 60
            self.camera.saturation = 25
            print('Starting PiCamera service, resolution: ', resolution, ' Framerate: ', framerate)

    def exposed_stop_service(self):
        print('{} :: Stopping Camera Service'.format(datetime.datetime.now()))
        if self.camera:
            if self.camera.previewing:
                self.camera.stop_preview()
            self.camera.close()
            del self.camera
        self.camera = None

    def exposed_take_picture(self, path, size=None):
        print('{} :: Taking picture - {}'.format(datetime.datetime.now(), path))
        self.camera.capture(path, format='jpeg', resize=size)

    def exposed_start_preview(self, x, y, width, height):
        print('{} :: Starting Preview - ({}, {}) - ({}, {})'.format(datetime.datetime.now(), x, y, width, height))
        if not self.camera.previewing:
            self.camera.start_preview(fullscreen=False, window=(x, y, width, height), layer=20)

    def exposed_stop_preview(self):
        print('{} :: Stopping Preview...'.format(datetime.datetime.now()))
        self.camera.stop_preview()

    def exposed_start_record(self, file_path):
        self.stop_record.clear()
        threading.Thread(target=self._record_video, args=(file_path, )).start()

    def _record_video(self, file_path):
        print('{} :: Starting video record in {} FPS - {}'.format(datetime.datetime.now(),
                                                                  self.camera.framerate, file_path))
        s_time = time.time()
        self.camera.start_recording(output=file_path, format='h264', level='4.2')
        self.camera.brightness = 60
        self.camera.saturation = 25
        
        while not self.stop_record.isSet():
            try:
                self.camera.wait_recording(.1)
            except PiCameraNotRecording:
                break
            if time.time() - s_time > MOTION_VIDEO_MAX_DURATION:
                break

        if self.camera.recording:
            self.camera.stop_recording()

    def exposed_stop_record(self):
        print('{} :: Stopping video record...'.format(datetime.datetime.now()))
        self.stop_record.set()

    def exposed_start_calibrate_service(self, resolution):
        if self.camera is None:
            print('Starting PiCamera service(Calibration), resolution: ', resolution)
            self.camera = PiCamera(resolution=resolution)
            self._restore_params()
            self.raw_capture = PiRGBArray(self.camera, size=self.camera.resolution)

    def _restore_params(self):
        for param in self.params.keys():
            setattr(self.camera, param, self.params[param])

    def exposed_set_param(self, param, value):
        if self.camera:
            if hasattr(self.camera, param):
                setattr(self.camera, param, value)
                self.params[param] = value
                print('Set Calibration Parameter: `{}` => `{}`'.format(param, value))

    def exposed_take_calibration_picture(self):
        """
        Take picture and save to ram-disk.
        :return:
        """
        if self.camera:
            self.camera.capture(self.raw_capture, format='bgr')
            frame = numpy.asarray(self.raw_capture.array)
            self.raw_capture.truncate(0)
            if os.path.exists(CALIBRATION_IMAGE_CURRENT):
                shutil.move(CALIBRATION_IMAGE_CURRENT, CALIBRATION_IMAGE_PREVIOUS)
            im = Image.fromarray(frame)
            im.save(CALIBRATION_IMAGE_CURRENT)

    def exposed_get_current_params(self):
        return self.params


def start_camera():

    server = ThreadedServer(CameraService, port=RPYC_CAMERA_PORT,
                            protocol_config={'allow_public_attrs': True, 'allow_pickle': True})
    server.start()
