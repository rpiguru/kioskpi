# -*- coding: iso8859-15 -*-

import base64
import re
import os
import time
import validate_email
from settings import SENDGRID_API_KEY, EMAIL_SENDER, MONGO_DB, MONGO_COLLECTION_EMAIL
from utils.store_selectors import CollageImagePath, CurEvent
import sendgrid
from sendgrid.helpers.mail import *
from kivy.logger import Logger
from pymongo import MongoClient


regex_email = re.compile(r'^[\w.\-]+@\w[\w.\-]*\.[a-zA-Z]{3,10}$')


def is_valid_email(email):
    """
    Validate email format.
    `validate_email` thinks that "aaaaa@bbbbb" is valid one, so use regex before this.
    :param email:
    :return:
    """
    m = regex_email.match(email)
    if m is None:
        return False
    return validate_email.validate_email(email)


def send_email(email='', image_path=None, api_key=SENDGRID_API_KEY):
    mongo_client = MongoClient('localhost')
    mongo_db = mongo_client[MONGO_DB]
    col_insert = mongo_db[MONGO_COLLECTION_EMAIL]

    if image_path is None:
        image_path = CollageImagePath().get()
    conf_data = CurEvent().get()['Email']
    if conf_data.get('SendEmail', '').lower() != 'true':
        Logger.warning('e-Mail: `SendEmail` of {} is disabled, skipping...'.format(CurEvent().get()['@id']))
        return
    Logger.info('email added to DB:{} - {} - {}'.format(conf_data['Subject'],email,conf_data['Body']))
    data = {
        'image_path': image_path,
        'subject': conf_data['Subject'],
        'to_email': email,
        'api_key': api_key,
        'sent': False,
        'body': conf_data['Body'],
        'allow_html': True if conf_data['AllowHtml'].lower() == 'true' else False,
    }
    col_insert.insert_one(data)


def email_service(*args):
    """
    Service that reads remaining emails and send when internet is on.
    :param args:
    :return:
    """
    mongo_client = MongoClient('localhost')
    mongo_db = mongo_client[MONGO_DB]
    col_pop = mongo_db[MONGO_COLLECTION_EMAIL]

    while True:
        doc_list = list(col_pop.find({"sent": False}))
        if doc_list:
            for doc in doc_list:
                try:
                    if doc['allow_html']:
                        html = '<html><body>{}</body></html>'.format(doc['body'])
                        content = Content(type_='text/html', value=html)
                    else:
                        content = Content(type_='text/plain', value=doc['body'])
                    _mail = Mail(from_email=Email(EMAIL_SENDER),
                                 subject=doc['subject'], to_email=Email(doc['to_email']), content=content)
                    image_path = doc['image_path']
                    if os.path.exists(image_path):
                        attachment = Attachment()
                        with open(image_path, 'rb') as f:
                            data = f.read()
                        attachment.content = base64.b64encode(data).decode('utf-8')
                        attachment.type = 'image/{}'.format(image_path.split('.')[-1])
                        attachment.filename = image_path.split('/')[-1]
                        attachment.disposition = 'attachment'
                        attachment.content_id = 'Collage Image'
                        _mail.add_attachment(attachment)
                    else:
                        Logger.warning('e-Mail: File not found - {}'.format(image_path))

                    sg = sendgrid.SendGridAPIClient(apikey=doc['api_key'])

                    response = sg.client.mail.send.post(request_body=_mail.get())
                    # print('Status Code: ', response.status_code)
                    # print('Body: ', response.body)
                    # print('Header: ', response.headers)
                    if response.status_code == 202:
                        Logger.info('An e-mail was sent to "{}" successfully.'.format(doc['to_email']))
                        col_pop.update_one({"_id": doc['_id']}, {"$set": {"sent": True}})
                except Exception as e:
                    Logger.error('Email: Failed to send email - {}'.format(e))
                    time.sleep(60)
        time.sleep(5)


if __name__ == '__main__':
    import multiprocessing

    multiprocessing.Process(target=email_service).start()

    send_email(email='nicole@tributekiosk.com', image_path='../assets/images/sample.jpg')

    while True:
        time.sleep(1)
