# -*- coding: iso8859-15 -*-

import time
from InstagramAPI import InstagramAPI
from pymongo import MongoClient
from kivy.logger import Logger
from settings import MONGO_DB, MONGO_COLLECTION_INSTAGRAM, INSTAGRAM_USER, INSTAGRAM_PWD


def post_image_to_instagram(img_path, caption='Kiosk Pi Image', user=INSTAGRAM_USER, pwd=INSTAGRAM_PWD):
    """
    Post an image to Instagram
    NOTE: This function blocks the main frame, so this should be called in another process.
    :param pwd:
    :param user:
    :param img_path:
    :param caption:
    :return:
    """
    mongo_client = MongoClient('localhost')
    mongo_db = mongo_client[MONGO_DB]
    col_insert = mongo_db[MONGO_COLLECTION_INSTAGRAM]

    data = {
        'image_path': img_path,
        'caption': caption,
        'user': user,
        'pwd': pwd,
        'time': time.time(),
        'sent': False
    }
    col_insert.insert_one(data)


def instagram_service():
    mongo_client = MongoClient('localhost')
    mongo_db = mongo_client[MONGO_DB]
    col_pop = mongo_db[MONGO_COLLECTION_INSTAGRAM]
    while True:
        doc_list = list(col_pop.find({"sent": False}))
        if doc_list:
            for doc in doc_list:
                try:
                    api = InstagramAPI(username=doc['user'], password=doc['pwd'])
                    api.login(force=True)
                    api.uploadPhoto(photo=doc['image_path'], caption=doc['caption'])
                    if api.LastResponse.status_code == 200:
                        Logger.info('Instagram: Successfully posted - {}'.format(doc['image_path']))
                        doc['sent'] = True
                        doc['sent_time'] = time.time()
                        col_pop.update_one({"_id": doc['_id']}, {"$set": {"sent": True}})
                except Exception as e:
                    Logger.error('Failed to post an image to Instagram - {}'.format(e))
                    time.sleep(60)
        time.sleep(5)


if __name__ == '__main__':
    import multiprocessing

    multiprocessing.Process(target=instagram_service).start()

    post_image_to_instagram(img_path='../assets/images/sample.jpg')

    while True:
        time.sleep(1)
