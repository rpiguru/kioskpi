# -*- coding: iso8859-15 -*-

import glob
import os
import time
import math
from kivy.logger import Logger
from settings import MAIN_SCREEN_COLS, MAIN_SCREEN_ROWS, IMAGE_THUMB_FILES, IMAGE_RECORDED_FILES, IMAGE_LAPSED, \
    IMAGE_PRINT_FILES, WIDTH, HEIGHT, BORDER
from PIL import Image
import multiprocessing
from utils.printer import print_file


def compose_collage_image(event_name, metadata, src_images, target_img_path):
    """
    Compose Event Image with selected images.
    :return:
    """
    s_time = time.time()
    template_path = 'events/{}/default.jpg'.format(event_name)
    overlay_path = 'events/{}/overlay.png'.format(event_name)

    try:
        background = Image.open(template_path)
        photos = metadata['photo']
        if type(photos) != list:    # If there is only one photo, this will be a dict, not the list...
            photos = [photos]
        for i, path in enumerate(src_images):
            img = Image.open(path).convert('RGBA')
            m = photos[i]

            # Crop image to keep ratio
            w, h, r = int(m['width']), int(m['height']), int(m['rotation'])
            resized_img = crop_image(img, w, h, path=None)
            # Rotate image
            rotated_img = resized_img.rotate(angle=360 - int(m['rotation']), resample=Image.BICUBIC, expand=1)
            # And paste!
            pos_y = int(m['starty']) + int(w * math.sin(math.radians(r)))
            background.paste(rotated_img, (int(m['startx']), pos_y), rotated_img)
            del rotated_img
            del resized_img
            del img

        overlay_img = Image.open(overlay_path)
        background.paste(overlay_img, (0, 0), overlay_img)
        del overlay_img
        background.save(target_img_path, dpi=(300, 300))
        background.close()
        del background
        Logger.info('Image Collage: Succeeded to compose a collage image - {}'.format(target_img_path))
        Logger.info('Image Collage: Elapsed - {}'.format(time.time() - s_time))
        if metadata['@active'] == 'true':
            multiprocessing.Process(target=print_collage_image, args=(target_img_path, metadata.get('layout'))).start()
        else:
            Logger.info('Image Collage: `active=false`, print is skipped.')

    except Exception as e:
        Logger.exception('Image Collage: Failed to compose collage image - {}'.format(e))


def crop_image(img, width, height, path=None):
    """
    Crop an image with original ratio.
    :param path: Full path of the source image.
    :param img: If None, read image with `path`
    :param width:
    :param height:
    :return:
    """
    if img is None:
        img = Image.open(path)
    orig_ratio = img.size[0] / img.size[1]
    ratio = width / height
    if ratio >= orig_ratio:
        new_height = img.size[0] * height // width
        start_y = (img.size[1] - new_height) // 2
        cropped_img = img.crop((0, start_y, img.size[0], start_y + new_height))
    else:
        new_width = img.size[1] * width // height
        start_x = (img.size[0] - new_width) // 2
        cropped_img = img.crop((start_x, 0, start_x + new_width, img.size[1]))
    resized_img = cropped_img.resize((width, height), Image.ANTIALIAS)
    return resized_img


def get_template_size(event_name):
    img = Image.open('events/{}/default.jpg'.format(event_name))
    width, height = img.size
    img.close()
    return width, height


def create_thumb_image(path):
    """
    Create small thumb image to be used in the START page.
    :param path:
    :return:
    """
    img = Image.open(path)
    thumb_img = img.resize((WIDTH // MAIN_SCREEN_COLS, HEIGHT // MAIN_SCREEN_ROWS), Image.ANTIALIAS)
    thumb_path = path.replace(IMAGE_RECORDED_FILES, IMAGE_THUMB_FILES)
    thumb_dir = os.path.dirname(thumb_path)
    if not os.path.exists(thumb_dir):
        os.makedirs(thumb_dir)
    thumb_img.save(thumb_path)


def create_preview_image(path):
    """
    Create the preview image of a video file with `play` icon
    :param path:
    :return:
    """
    img = Image.open(path)
    icon = Image.open('assets/images/media_play.png').convert('RGBA')
    img.paste(icon, (int((img.size[0] - icon.size[0]) / 2), int((img.size[1] - icon.size[1]) / 2)), icon)
    img.save(path)
    create_thumb_image(path)


def print_collage_image(img_path, layout, border_size=24):
    if BORDER:
        new_img_path = '{}_bordered.jpg'.format('.'.join(img_path.split('.')[:-1]))
        img = Image.open(img_path)
        new_img = Image.new('RGB', (img.size[0] + border_size * 2, img.size[1] + border_size * 2))
        new_img.paste(img, (border_size+2, border_size))
        new_img.save(new_img_path, dpi=(300, 300))
    else:
        new_img_path = img_path
    if layout == '2x6':
        img = Image.open(new_img_path)
        new_img = Image.new('RGB', (img.size[0] * 2, img.size[1]))
        new_img.paste(img, (0, 0))
        new_img.paste(img, (img.size[0], 0))
        img.close()
        new_img.save(new_img_path, dpi=(300, 300))
    print_file(file_name=new_img_path, layout=layout)


def verify_image(img_path):
    try:
        img = Image.open(img_path)
        img.verify()
        img.close()
        return True
    except (IOError, SyntaxError):
        return False


def verify_event_images(event_name):
    template_path = 'events/{}/default.jpg'.format(event_name)
    overlay_path = 'events/{}/overlay.png'.format(event_name)
    if verify_image(template_path) and verify_image(overlay_path):
        # Start verification of captured images & time-lapsed images
        multiprocessing.Process(target=_verify_all_images, args=(event_name, )).start()
        return True
    else:
        return False


def get_preview_size_pos(img_path, offset_x=100):
    w, h = Image.open(img_path).size
    width = WIDTH - offset_x * 2
    height = width * h / w
    y = (HEIGHT - height) / 2
    return {'size': (width, height), 'pos': (offset_x, y)}


def _verify_all_images(event_name):
    Logger.info('Event: Starting verification of ALL images of {}'.format(event_name))

    thumb_dir = 'events/{}/{}/'.format(event_name, IMAGE_THUMB_FILES)
    if not os.path.exists(thumb_dir):
        os.makedirs(thumb_dir)

    for base in [IMAGE_RECORDED_FILES, IMAGE_LAPSED, IMAGE_PRINT_FILES]:
        for img in glob.glob('events/{}/{}/*.jpg'.format(event_name, base)):
            if not verify_image(img):
                Logger.error('Image: {} is corrupted, deleting...'.format(img))
                try:
                    os.unlink(img)
                except Exception as e:
                    Logger.error('Image: Failed to delete {} - {}'.format(img, e))
            else:
                create_thumb_image(img)
