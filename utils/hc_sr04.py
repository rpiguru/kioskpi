# -*- coding: iso8859-15 -*-

import RPi.GPIO as GPIO
import time

timeout = 5


class DistanceReader:

    trig_pin = 18
    echo_pin = 24

    def __init__(self, TRIG=18, ECHO=24):
        self.trig_pin = TRIG
        self.echo_pin = ECHO
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.trig_pin, GPIO.OUT)                  # Set pin as GPIO out
        GPIO.setup(self.echo_pin, GPIO.IN)                   # Set pin as GPIO in

    def read_distance(self):
        GPIO.output(self.trig_pin, True)                     # Set TRIG as HIGH
        time.sleep(.00001)                                   # Delay of 0.00001 seconds
        GPIO.output(self.trig_pin, False)                    # Set TRIG as LOW

        s_time = time.time()
        pulse_start = time.time()
        while GPIO.input(self.echo_pin) == 0:                # Check whether the ECHO is LOW
            pulse_start = time.time()                        # Saves the last known time of LOW pulse
            if time.time() - s_time > timeout:
                print('ECHO is always LOW!, skipping')
                return 0

        s_time = time.time()
        pulse_end = time.time()
        while GPIO.input(self.echo_pin) == 1:                # Check whether the ECHO is HIGH
            pulse_end = time.time()                          # Saves the last known time of HIGH pulse
            if time.time() - s_time > timeout:
                print('ECHO is always HIGH!, skipping')
                return 0

        pulse_duration = pulse_end - pulse_start             # Get pulse duration to a variable

        distance = pulse_duration * 17150                    # Multiply pulse duration by 17150 to get distance
        distance = round(distance, 2)                        # Round to two decimal points

        if distance > 500:
            return 0
        else:
            return distance - .5


if __name__ == '__main__':
    try:
        reader = DistanceReader()
        while True:
            dist = reader.read_distance()
            print("Measured Distance = %.1f cm" % dist)
            time.sleep(1)
    except KeyboardInterrupt:
        print("Measurement stopped by User")
        GPIO.cleanup()
