# -*- coding: iso8859-15 -*-

# ========== Constant Values for Camera Calibration ==========

AWB_MODE = ['auto', 'off', 'sunlight', 'cloudy', 'shade', 'tungsten', 'fluorescent', 'incandescent', 'flash', 'horizon']

DRC_MODE = ['off', 'low', 'medium', 'high']

EXPOSURE_MODE = ['auto', 'off', 'night', 'nightpreview', 'backlight', 'spotlight', 'sports',
                 'snow', 'beach', 'verylong', 'fixedfps', 'antishake', 'fireworks']

IMAGE_EFFECT = ['none', 'negative', 'solarize', 'sketch', 'denoise', 'emboss', 'oilpaint', 'hatch', 'gpen', 'pastel',
                'watercolor', 'film', 'blur', 'saturation', 'colorswap', 'washedout', 'posterise', 'colorpoint',
                'colorbalance', 'cartoon', 'deinterlace1', 'deinterlace1']

METER_MODE = ['average', 'spot', 'backlit', 'matrix']
