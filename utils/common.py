# -*- coding: iso8859-15 -*-

import os
import subprocess
import signal
import time
import gc


def get_serial():
    """
    Get serial number of the device
    :return:
    """
    if is_rpi():
        cpuserial = "0000000000000000"
        try:
            f = open('/proc/cpuinfo', 'r')
            for line in f:
                if line[0:6] == 'Serial':
                    cpuserial = line[10:26].lstrip('0')
            f.close()
        except:
            cpuserial = "ERROR000000000"
        return cpuserial
    else:
        # return '838e39e3'
        return '12345678'


def get_image_name(event_name, base_path, suffix='', ext='jpg'):
    """
    Compose target image file name based on the path
    :param suffix:
    :param event_name:
    :param base_path:
    :param ext:
    :return:
    """
    img_dir = 'events/{}/{}'.format(event_name, base_path)
    if not os.path.exists(img_dir):
        os.makedirs(img_dir)
    return os.path.join(img_dir, '{}-{}{}.{}'.format(get_serial(), int(time.time()), suffix, ext))


def is_rpi():
    return 'arm' in os.uname()[4]


def kill_process_by_name(proc_name):
    p = subprocess.Popen(['ps', '-A'], stdout=subprocess.PIPE)
    out, err = p.communicate()
    for line in out.decode().splitlines():
        if proc_name in line:
            pid = int(line.split(None, 1)[0])
            print('Found PID({}) of `{}`, killing...'.format(pid, proc_name))
            os.kill(pid, signal.SIGKILL)


def number_to_ordinal(n):
    """
    Convert number to ordinal number string
    """
    return "%d%s" % (n, "tsnrhtdd"[(n / 10 % 10 != 1) * (n % 10 < 4) * n % 10::4])


def get_free_gpu_size():
    gc.collect()
    if is_rpi():
        pipe = os.popen('sudo vcdbg reloc stats | grep "free memory"')
        data = pipe.read().strip()
        pipe.close()
        return data
    else:
        return 0


def disable_screen_saver():
    os.system('sudo sh -c "TERM=linux setterm -blank 0 >/dev/tty0"')


def get_screen_resolution():
    """
    Get resolution of the screen
    :return:
    """
    if is_rpi():
        pipe = os.popen('fbset -s')
        data = pipe.read().strip()
        pipe.close()
        for line in data.splitlines():
            if line.startswith('mode'):
                w, h = [int(p) for p in line.split('"')[1].split('x')]
                return w, h
    else:
        return 1366, 768


def check_running_proc(proc_name):
    """
    Check if a process is running or not
    :param proc_name:
    :return:
    """
    is_running = False
    try:
        if len(os.popen("ps -aef | grep -i '%s' "
                        "| grep -v 'grep' | awk '{ print $3 }'" % proc_name).read().strip().splitlines()) > 0:
            is_running = True
    except Exception as e:
        print('Failed to get status of the process({}) - {}'.format(proc_name, e))
    return is_running
