#!/usr/bin/python3
# -*- coding: iso8859-15 -*-

import os
import multiprocessing
import sys


if not sys.version_info >= (3, 0):
    sys.stdout.write("Sorry, requires Python 3.x, not Python 2.x. Please execute with `python3 main.py`\n")
    sys.exit(1)

app_dir = os.path.abspath(os.path.dirname(__file__))
if app_dir not in sys.path:
    sys.path.append(app_dir)


multiprocessing.freeze_support()


def camera_worker():
    import utils.camera
    utils.camera.start_camera()


from utils.common import is_rpi

if is_rpi():
    camera_process = multiprocessing.Process(target=camera_worker)
    camera_process.start()


# Import this before kivy
import conf.config_before
from kivy.app import App
import conf.config_kivy
import widgets.factory_reg

import datetime
import rpyc
import glob
import gc
import time
import subprocess
import traceback
from kivy.uix.screenmanager import SlideTransition, NoTransition
from kivymd.theming import ThemeManager
from screens.screen_manager import screens, sm
from kivy.logger import Logger
from kivy.clock import Clock
from kivy.base import ExceptionHandler, ExceptionManager
from utils.e_mail import email_service
from utils.instagram import instagram_service
from utils.store_selectors import PreviousScreen, SelectedImages, CurEvent, get_current_event_name
from utils.twitter import twitter_service
from utils.common import get_free_gpu_size, disable_screen_saver, get_image_name, check_running_proc
from utils.config import KioskConfigManager
from settings import *


class KioskExceptionHandler(ExceptionHandler):

    def handle_exception(self, exception):
        Logger.exception(exception)
        _app = App.get_running_app()
        _app.save_exception(traceback.format_exc(limit=20))
        _app.switch_screen('error')
        return ExceptionManager.PASS


ExceptionManager.add_handler(KioskExceptionHandler())


class PhotosApp(App):

    theme_cls = ThemeManager()
    current_screen = None
    exception = None
    prev_screen_name = None

    camera = None
    connection = None

    def build(self):
        CurEvent().subscribe(self.on_event_changed)
        CurEvent().set_default()
        self.title = 'Kiosk Pi'
        if is_rpi():
            self.connection = rpyc.connect("localhost", RPYC_CAMERA_PORT)
            self.camera = self.connection.root
            # self.camera.start_service(sensor_mode=1, resolution=(PICAMERA_IMAGE_WIDTH, PICAMERA_IMAGE_HEIGHT),
            #                           framerate=30)
        Clock.schedule_interval(lambda dt: self.take_time_lapsed_image(), TIME_LAPSED_INTERVAL)
        self.switch_screen(INIT_SCREEN)
        return sm

    def switch_screen(self, screen_name, direction=None, duration=.3):
        if sm.has_screen(screen_name):
            sm.current = screen_name
        else:
            s_time = time.time()
            if is_rpi():
                cmd = 'omxplayer -o local assets/sounds/switch_screen.mp3'
                subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            screen = screens[screen_name](name=screen_name)
            if direction:
                sm.transition = SlideTransition(direction=direction, duration=duration)
            else:
                sm.transition = NoTransition()
            PreviousScreen().set(sm.current)

            if is_rpi():
                if sm.current == 'action_video':
                    self.camera.stop_service()
                    self.camera.start_service(sensor_mode=1, resolution=(PICAMERA_IMAGE_WIDTH, PICAMERA_IMAGE_HEIGHT),
                                              framerate=30)
                elif screen_name == 'action_video':
                    self.camera.stop_service()
                    self.camera.start_service(sensor_mode=6, resolution=(PICAMERA_RECORD_WIDTH, PICAMERA_RECORD_HEIGHT),
                                              framerate=PICAMERA_RECORD_FRAME_RATE)

            sm.switch_to(screen)
            msg = ' :: GPU - {}'.format(get_free_gpu_size()) if is_rpi() else ''
            Logger.info('Kiosk: === Switched to {} screen {}, elapsed: {}'.format(
                screen_name, msg, time.time() - s_time))
            if self.current_screen:
                sm.remove_widget(self.current_screen)
                del self.current_screen
                gc.collect()
            self.current_screen = screen

    def take_time_lapsed_image(self):
        cur_event_name = get_current_event_name()
        if cur_event_name != 'default':
            lapsed_dir = 'events/{}/{}/'.format(cur_event_name, IMAGE_LAPSED)
            if not os.path.exists(lapsed_dir):
                os.makedirs(lapsed_dir)
            img_path = get_image_name(event_name=cur_event_name, base_path=IMAGE_LAPSED)
            try:
                Logger.debug('Time-lapsed: {} :: Taking picture - {}'.format(datetime.datetime.now(), img_path))
                if is_rpi():
                    self.camera.take_picture(img_path, size=(PICAMERA_IMAGE_WIDTH, PICAMERA_IMAGE_HEIGHT))
            except Exception as e:
                Logger.error('Time-lapsed: Failed to take picture - {}'.format(e))

    def save_exception(self, ex):
        self.exception = ex

    def get_exception(self):
        return self.exception

    def stop(self, *largs):
        p = subprocess.Popen(['sudo', 'systemctl', 'stop', 'kioskpi'])
        p.communicate()
        p.wait()
        super(PhotosApp, self).stop(*largs)
        os.kill(os.getpid(), 9)

    @staticmethod
    def on_event_changed(*args):
        SelectedImages().set(glob.glob('events/{}/{}/*.jpg'.format(get_current_event_name(), IMAGE_RECORDED_FILES)))


if __name__ == '__main__':

    Logger.debug('=============== Starting KioskPi ===============')
    if not check_running_proc('mongo') and is_rpi():
        Logger.warning('Kiosk: MongoDB service is not running... repairing...')
        _p = subprocess.Popen('sudo -u mongodb mongod --repair --dbpath /var/lib/mongodb/', shell=True,
                              stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        _p.communicate()
        _p.wait()
        subprocess.Popen('service mongodb start', shell=True)

    disable_screen_saver()

    if UPDATE_EVENT:
        config_manager = KioskConfigManager()
        config_manager.start()

    multiprocessing.Process(target=email_service).start()
    multiprocessing.Process(target=instagram_service).start()
    multiprocessing.Process(target=twitter_service).start()

    app = PhotosApp()

    try:
        app.run()
    except KeyboardInterrupt:
        app.stop()
