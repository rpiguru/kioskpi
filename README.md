# Kiosk Pi

## Installation

Execute installation script:

    bash install.sh

NOTE: RPi will reboot after this script.


## Update KioskPi

    bash update.sh

NOTE: RPi will reboot after this script.
